FROM registry.redhat.io/redhat-openjdk-18/openjdk18-openshift:latest
COPY spring-hello-*.war /app.war
# specify default command
CMD ["/usr/bin/java", "-jar", "-Dspring.profiles.active=test", "/app.war"]